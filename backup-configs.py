#!/usr/bin/env python3

from collections.abc import Iterable, Sequence
import sys, subprocess, os, shutil
from pathlib import Path

dryrun = False

# returns path.joinpath(endpath) for each endpath in pathlist
# path is pathlib.Path, pathlist is a list of Path
def join_iter(path: Path, pathlist: Iterable[Path]):
    if isinstance(path, str):
        path = Path(path)
    for endpath in pathlist:
        # no need to join if it is already part of it
        if not str(endpath).startswith(str(path)):
            if endpath.root == "/":
                endpath = endpath.relative_to("/")
            endpath = path.joinpath(endpath)
        yield endpath

# delete everything in path except files/directories in exceptions and file/subdirectories of directories in exceptions
# path is an absolute Path, exceptions is a list of absolute Path
def delete(path: Path, exceptions: Iterable[Path]):
    no_exception = True
    for exception in exceptions:
        if path == exception:
            return
        elif path in exception.parents:
            no_exception = False
    if no_exception:
        if path.is_dir():
            print("rmtree({})".format(path))
            if not dryrun:
                shutil.rmtree(str(path))
        else:
            print("unlink({})".format(path))
            if not dryrun:
                path.unlink()
    elif path.is_dir():
        for entry in path.iterdir():
            delete(entry, exceptions)

def process_args_str(args: Iterable[str]):
    ret_str = ""
    for arg in args:
        arg = arg.replace("\\", "\\\\")
        if " " in arg or "$" in arg or "{" in arg:
            arg = "'{}'".format(arg)
        ret_str += "{} ".format(arg)
    return ret_str[:-1]

def run_process(args: Sequence[str]):
    args_str = process_args_str(args)
    print(args_str)
    if dryrun:
        return

    proc = subprocess.run(args)
    if proc.returncode != 0:
        print("Non-zero returncode: {}".format(args_str), sys.stderr)
        sys.exit(proc.returncode)

# this function uses rsync to copy src to dest_root/src
# creates directories and corrects permissions if necessary
def backup(src: Path, dest_root: Path):
    destparent = dest_root
    for srcparent in reversed(src.parents):
        destparent = dest_root.joinpath(srcparent.relative_to("/"))
        srcparent_stat = srcparent.stat()
        if not destparent.exists():
            print("mkdir {}".format(destparent))
            if not dryrun:
                destparent.mkdir()
        if destparent.exists():
            destparent_stat = destparent.stat()
            if srcparent_stat.st_mode != destparent_stat.st_mode:
                print("chmod {} {}".format(oct(srcparent_stat.st_mode)[4:], destparent))
                if not dryrun:
                    destparent.chmod(srcparent_stat.st_mode)
            if (srcparent_stat.st_uid != destparent_stat.st_uid) or (srcparent_stat.st_gid != destparent_stat.st_gid):
                print("chown {}:{} {}".format(srcparent.owner(), srcparent.group(), destparent))
                if not dryrun:
                    os.chown(str(destparent), srcparent_stat.st_uid, srcparent_stat.st_gid)

    run_process(["rsync", "-a", "--delete", str(src), str(destparent)])

def read_config():
    dest_root = ""
    srcpaths = []
    path_beginning = None
    config_path = "/etc/backup-configs/backup-configs.conf"
    with open(config_path, "r") as f:
        for line in f:
            line = line.strip()
            if (not line) or (line[0] == "#"):
                continue
            if (path_beginning is None) and line.startswith("dest_root="):
                dest_root = Path(line[len("dest_root="):])
            elif line[0] == "[":
                path_beginning = Path(line[1:-1])
            else:
                assert path_beginning is not None
                srcpaths.append(path_beginning.joinpath(line))

    if not dest_root:
        raise Exception("Please add dest_root= to the beginning of the config file at {}".format(config_path))
    if not srcpaths:
        raise Exception("No paths found in config file at {}".format(config_path))

    return srcpaths, dest_root

def tar(dest_root: Path):
    archive_dir = dest_root.parent
    archive_name = "{}.configs.tar.zst".format(archive_dir.name)
    filenames = [child.name for child in archive_dir.iterdir() if (child.name != archive_name) and (not child.name.startswith("."))]
    run_process(["tar", "--zstd", "-cf", str(archive_dir.joinpath(archive_name)), "-C", str(archive_dir), *filenames])

def main():
    global dryrun

    for arg in sys.argv[1:]:
        if arg == "-n" or arg == "--dry-run":
            dryrun = True
        else:
            print("Usage: sudo {} [--dry-run]".format(sys.argv[0]))
            return

    # read out the config
    srcpaths, dest_root = read_config()

    # backup with rsync
    for src in srcpaths:
        backup(src, dest_root)

    # delete files/directories that are not in config
    delete(dest_root, list(join_iter(dest_root, srcpaths)))

    # create tar archive
    tar(dest_root)

if __name__ == "__main__":
    main()
